# Singularity Runner +GPU
The Singularity Runner is a demo deployment for Google Cloud Platform that provisions a single Compute Engine instance with GPUs and the necessary tools for running GPU accelerated [Singularity](https://sylabs.io/singularity/) containers.

## Getting Started
A new singularity runner deployment is launched using Google Cloud's Deployment Manager
```
$ gcloud deployment-manager deployments create srunner --config=runner.yaml
```

The file, `runner.yaml` defines the following parameters for the deployment :
```
machine_type : GCP machine type, (e.g. n1-standard-4)
gpu_count    : Number of GPUs, only 1, 2, 4, or 8
gpu_type     : GPU Type (e.g. nvidia-tesla-v100). 
region       : GCP Region where the compute engine resource will be created (e.g. us-west1-b)
zone         : GCP Zone within the region where the compute engine resource will be created (e.g. us-west1)
```
For more information on allowed GPU types and the zones in GCP where they are offered,See [GPU Types on GCP documentation](https://cloud.google.com/compute/docs/gpus/#gpus-list) 

Before deploying, you should modify the `runner.yaml` script to fit your needs.
For a more detailed description of the deployment parameters, see the [singularity-runner.py.schema](singularity-runner.py.schema).

## Running a singularity container with GPU acceleration
Once you've deployed the singularity runner instance, you can ssh into the instance from the compute engine UI panel in GCP. On this instance,
download your singularity container. To run your container and expose it to the GPU and drivers installed on this system,
```
singularity run --nv my-container.sif
```
Keep in mind that your Singularity container image must contain an application with CUDA compiled code.

