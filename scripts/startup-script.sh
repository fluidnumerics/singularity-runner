#!/bin/bash


# Setup selinux
    setenforce 0
    cat 'SELINUX=permissive' > /etc/selinux/config
    cat 'SELINUXTYPE=targeted' >> /etc/selinux/config


# Install packages
    yum groupinstall -y 'Development Tools'

    yum install -y bind-utils \
                epel-release \
                gcc \
                git \
                numactl \
                numactl-devel \
                openssl-devel \
                libuuid-devel \
                libseccomp-devel \
                squashfs-tools \
                vim \
                wget 

# Install go
    if [ ! -d /usr/local/go ]; then
        cwd=$(pwd)
        cd /tmp
        
        curl -LO https://storage.googleapis.com/golang/go1.12.6.linux-amd64.tar.gz
        tar -C /usr/local -xvzf go1.12.6.linux-amd64.tar.gz
        echo 'export PATH=$PATH:/usr/local/go/bin' > /etc/profile.d/gopath.sh
        
        export HOME="/apps"
        export GOPATH="/apps/go"
        mkdir -p /apps/go
        export PATH=$PATH:/usr/local/go/bin
        go get -u github.com/golang/dep/cmd/dep
        cd $cwd
    fi


# install singularity
    if [ ! -d /apps/go/src/github.com/sylabs ]; then
        mkdir -p /apps/go/src/github.com/sylabs/

        git clone --branch v3.2.1 --depth 1 https://github.com/sylabs/singularity /apps/go/src/github.com/sylabs/singularity

        cd /apps/go/src/github.com/sylabs/singularity && \
        ./mconfig && \
        cd ./builddir && \
        make && \
        make install

        cd $cwd
    fi

# Install nvidia drivers

    if [ ! -d /usr/local/cuda ]; then
        echo "CUDA_PATH=/usr/local/cuda" > /etc/profile.d/cuda.sh
        echo "PATH=$CUDA_PATH/bin${PATH:+:${PATH}}" >> /etc/profile.d/cuda.sh
        echo "LD_LIBRARY_PATH=$CUDA_PATH/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}" >> /etc/profile.d/cuda.sh

        yum -y install kernel-devel-$(uname -r) kernel-headers-$(uname -r)
        wget http://developer.download.nvidia.com/compute/cuda/repos/rhel7/x86_64/cuda-repo-rhel7-10.0.130-1.x86_64.rpm
        rpm -i cuda-repo-rhel7-10.0.130-1.x86_64.rpm
        yum clean all
        yum -y install cuda

        # Apparently, on CentOS 7, device files for GPUs are created dynamically.
        # To have them available for use with singularity containers, both nvidia-smi
        # and another GPU application need to be run on the host to create  (particularly)
        # /dev/nvidia_uvm
        # See https://github.com/sylabs/singularity/issues/1441
        systemctl enable nvidia-persistenced
        systemctl start nvidia-persistenced

        /sbin/modprobe nvidia-uvm
        if [ "$?" -eq 0 ]; then
          # Find out the major device number used by the nvidia-uvm driver
          D=`grep nvidia-uvm /proc/devices | awk '{print $1}'`
        
          mknod -m 666 /dev/nvidia-uvm c $D 0
        else
          exit 1
        fi

    fi
