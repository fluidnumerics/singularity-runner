# Copyright 2019 Fluid Numerics LLC.
#
# Modified for use with the Singularity-Runner deployment
# See LICENSE included in this repository.
#
# Copyright 2016 Google Inc. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Creates a GCE Instance with tools installed for running GPU Accelerated Singularity Containers."""
COMPUTE_URL ='https://www.googleapis.com/compute/v1/projects/'

def GenerateConfig(context):

    resources = []
    resources.append({ 'name': 'singularity-runner',
                       'type': 'compute.v1.instance',
                       'properties':{
                          'guestAccelerators':[
                              {'acceleratorType': COMPUTE_URL+context.env["project"]+
                                                 "/zones/"+context.properties["zone"]+
                                                 "/acceleratorTypes/"+context.properties["gpu_type"],
                               'acceleratorCount':context.properties["gpu_count"]
                              }
                            ],
                          'scheduling':{'onHostMaintenance': 'TERMINATE'},
                          'zone': context.properties['zone'],
                          'tags': { 'items' : ['http-server','https-server'] },
                          'machineType': COMPUTE_URL+context.env['project']+'/zones/'+context.properties['zone']+'/machineTypes/'+context.properties['machine_type'],
                          'disks':[{
                              'deviceName':'boot',
                              'type': 'PERSISTENT',
                              'boot': True,
                              'autoDelete': True,
                              'initializeParams':{
                                  'sourceImage': COMPUTE_URL+'centos-cloud/global/images/family/centos-7',
                                  'diskType': COMPUTE_URL+context.env['project']+'/zones/'+context.properties['zone']+'/diskTypes/'+context.properties['disk_type'],
                                  'diskSizeGb': context.properties['disk_size_gb']
                                  }
                              }],
                              'canIpForward': True,
                              'networkInterfaces':[ {'subnetwork':COMPUTE_URL+
                                                      context.env["project"]+
                                                      '/regions/'+context.properties["region"]+
                                                      '/subnetworks/default',
                                                      'accessConfigs': [ {'name':'External NAT', 'type':'ONE_TO_ONE_NAT'} ]
                                                      }],
                              'serviceAccounts': [ {'email':'default', 'scopes':["https://www.googleapis.com/auth/cloud-platform"]} ],
                              'metadata' : {
                                  'items': [ {'key':'startup-script','value': context.imports['scripts/startup-script.sh']},
                                             {'key':'enable-oslogin','value': 'TRUE'}
                                           ]
                                  },
                       }
                    })

    return {'resources': resources}

